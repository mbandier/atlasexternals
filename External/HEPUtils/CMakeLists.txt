# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building HEPUtils as part of the offline/analysis software.
#

# Declare the name of the package:
atlas_subdir( HEPUtils )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source code of HEPUtils:
set( _heputilsSource "http://cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/" )
set( _heputilsSource "${_heputilsSource}heputils-1.3.2.tar.gz" )
set( _heputilsMd5 "b001e9462f3a575c0c585a8445d71914" )

# Temporary directory for the build results:
set( _buildDir
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HEPUtilsBuild )

# "Build" HEPUtils:
ExternalProject_Add( HEPUtils
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_heputilsSource}
   URL_MD5 ${_heputilsMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the installation of HEPUtils"
   BUILD_COMMAND make install PREFIX=${_buildDir}
   INSTALL_COMMAND make install PREFIX=<INSTALL_DIR> )
add_dependencies( Package_HEPUtils HEPUtils )

# Install MCUtils:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
