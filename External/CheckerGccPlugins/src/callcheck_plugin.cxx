/**
 * @file CheckerGccPlugins/src/callcheck_plugin.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2019
 * @brief Various ATLAS-specific function call checks.
 *
 * Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */


#include "checker_gccplugins.h"
#include "tree.h"
#include "function.h"
#include "basic-block.h"
#include "coretypes.h"
#include "is-a.h"
#include "predict.h"
#include "internal-fn.h"
#include "tree-ssa-alias.h"
#include "gimple-expr.h"
#include "gimple.h"
#include "gimple-iterator.h"
#include "tree-ssa-loop.h"
#include "cp/cp-tree.h"
#include "diagnostic.h"
#include "context.h"
#include "tree-pass.h"
#include "gimple-pretty-print.h"
#include "print-tree.h"
#include "tree-cfg.h"
#include "cfgloop.h"
#include "tree-ssa-operands.h"
#include "tree-phinodes.h"
#include "gimple-ssa.h"
#include "ssa-iterators.h"
#include "stringpool.h"
#include "attribs.h"
#include "gcc-rich-location.h"
#include <vector>
#include <unordered_map>
#include <string>


using namespace CheckerGccPlugins;


namespace {


const char* url = "<https://gitlab.cern.ch/atlas/atlasexternals/tree/master/External/CheckerGccPlugins#callcheck_plugin>";

enum Fntype
{
 INSERT,
 APPEND,
 PREFIX,
 CURCTX,
 RNG,
};


std::unordered_map<std::string, Fntype> context_fns = {
 { "SG::ReadHandle<>::ReadHandle(const SG::ReadHandleKey<>&)",               APPEND },
 { "SG::makeHandle(const SG::ReadHandleKey<>&)",                             APPEND },
 //{ "SG::ReadHandle<>::get() const",                                          INSERT },
 { "SG::WriteHandle<>::WriteHandle(const SG::WriteHandleKey<>&)",            APPEND },
 { "SG::makeHandle(const SG::WriteHandleKey<>&)",                            APPEND },
 //{ "SG::WriteHandle<>::put(std::unique_ptr<>, bool) const",                  PREFIX },
 //{ "SG::WriteHandle<>::put(SG::DataObjectSharedPtr<>) const",                PREFIX },
 //{ "SG::WriteHandle<>::put(std::unique_ptr<>, std::unique_ptr<>) const",     PREFIX },
 { "SG::UpdateHandle<>::UpdateHandle(const SG::UpdateHandleKey<>&)",         APPEND },
 { "SG::makeHandle(const SG::UpdateHandleKey<>&)",                           APPEND },
 { "SG::ReadCondHandle<>::ReadCondHandle(const SG::ReadCondHandleKey<>&)",   APPEND },
 { "SG::WriteCondHandle<>::WriteCondHandle(const SG::WriteCondHandleKey<>&)",APPEND },
 { "SG::ReadDecorHandle<>::ReadDecorHandle(const SG::ReadDecorHandleKey<>&)",APPEND },
 { "SG::makeHandle(const SG::ReadDecorHandleKey<>&)",                        APPEND },
 { "SG::WriteDecorHandle<>::WriteDecorHandle(const SG::WriteDecorHandleKey<>&)",APPEND},
 { "SG::makeHandle(const SG::WriteDecorHandleKey<>&)",                       APPEND },
 { "SG::ReadMetaHandle<>::ReadMetaHandle(const SG::ReadMetaHandleKey<>&)",   APPEND },
 { "SG::WriteMetaHandle<>::WriteMetaHandle(const SG::WriteMetaHandleKey<>&)",APPEND },
 { "SG::HandleKeyArray<>::makeHandles() const",                              INSERT },
 { "ICoreDumpSvc::setCoreDumpInfo(const string&, const string&)",            PREFIX },
 { "IDecisionSvc::isEventAccepted(const string&) const",                     APPEND },
 { "Athena::IRCUObject::quiescent()",                                        INSERT },
 { "Athena::RCUObject<>::readerQuiesce()",                                   INSERT },
 { "Athena::RCUObject<>::updater()",                                         INSERT },
 { "Athena::RCUReadQuiesce<>::RCUReadQuiesce(Athena::RCUObject<>&)",         APPEND },
 { "Athena::RCUUpdate<>::RCUUpdate(Athena::RCUObject<>&)",                   APPEND },
 { "SG::SlotSpecificObj<>::get()",                                           INSERT },
 { "SG::SlotSpecificObj<>::get() const",                                     INSERT },
 { "Athena::Timeout::instance()",                                            INSERT },
 { "Gaudi::Hive::currentContext()",                                          CURCTX },
 { "ATHRNG::RNGWrapper::operator CLHEP::HepRandomEngine*() const",           RNG    },
};


bool is_eventcontext (tree typ)
{
  static const_tree ectyp = NULL_TREE;
  if (ectyp) {
    return typ == ectyp;
  }

  if (CP_TYPE_CONTEXT (typ) != global_namespace) return false;
  tree id = TYPE_IDENTIFIER (typ);
  if (!id) return false;
  const char* name = IDENTIFIER_POINTER (id);
  if (strcmp (name, "EventContext") == 0) {
    ectyp = typ;
    return true;
  }
  return false;
}


tree context_arg (const_tree fndecl)
{
  {
    std::string pstr;
    tree parms = DECL_ARGUMENTS (fndecl);
    for (; parms; parms = TREE_CHAIN (parms)) {
      tree arg_type = TREE_TYPE (parms);
      if (TREE_CODE (arg_type) == POINTER_TYPE) {
        arg_type = TREE_TYPE (arg_type);
        pstr = "*";
      }
      else if (POINTER_TYPE_P (arg_type)) {
        arg_type = TREE_TYPE (arg_type);
      }
      if (is_eventcontext (arg_type)) {
        return parms;
      }
    }
  }
  return NULL_TREE;
}


void hide_targs (std::string& s)
{
  std::string::size_type istart = 0;
  std::string::size_type i = 0;
  int nest = 0;
  for (; i < s.size(); ++i) {
    if (s[i] == '<') {
      if (nest == 0) {
        istart = i;
      }
      ++nest;
    }
    else if (s[i] == '>' && nest > 0) {
      --nest;
      if (nest == 0) {
        s.erase (istart+1, i-(istart+1));
        i = istart+1;
      }
    }
  }
}


void add_fixit (gcc_rich_location& richloc,
                Fntype fntype,
                const std::string& parmname)
{
  if (parmname.empty()) return;
  source_range r = get_range_from_loc (line_table,
                                       richloc.get_range(0)->m_loc);
  switch (fntype) {
  case INSERT:
    richloc.add_fixit_insert_before (r.m_finish, parmname.c_str());
    break;

  case PREFIX:
    richloc.add_fixit_insert_after (get_pure_location (richloc.get_loc()),
                                    (parmname + ", ").c_str());
    break;

  case APPEND:
    richloc.add_fixit_insert_before (r.m_finish,
                                     (", " + parmname).c_str());
    break;

  default:
    break;
  }
}


void emit_warning (Fntype fntype,
                   gimplePtr stmt,
                   tree fndecl,
                   tree ctx_parm)
{
  gcc_rich_location richloc (gimple_location (stmt));

  std::string parmname;
  tree id = TREE_VALUE (ctx_parm);
  if (id) {
    parmname = IDENTIFIER_POINTER (id);
    if (TREE_CODE (TREE_TYPE (ctx_parm)) == POINTER_TYPE) {
      parmname = "*" + parmname;
    }
  }

  bool warned = false;
  switch (fntype) {
  case INSERT:
  case APPEND:
  case PREFIX:
    add_fixit (richloc, fntype, parmname);
    warned = warning_at (&richloc, 0,
                         "Call to %qD should include EventContext argument.",
                         fndecl);
    break;

  case CURCTX:
    warned = warning_at (&richloc, 0,
                         "Replace call to %qD with parameter",
                         fndecl);
    break;

  case RNG: {
    std::string fixit = ").get(" + parmname + ")";
    richloc.add_fixit_insert_before ("(");
    richloc.add_fixit_insert_after (fixit.c_str());
    warned = warning_at (&richloc, 0,
                         "Change conversion %qD to getEngine, passing EventContext.",
                         fndecl);
    break;
  }

  default:
    break;
  }

  if (warned) {
    inform (DECL_SOURCE_LOCATION (ctx_parm), "EventContext was passed to this function here.");
    CheckerGccPlugins::inform_url (gimple_location (stmt), url);
  }
}


bool whitelisted (tree fndecl)
{
  std::string name = decl_as_string (fndecl, TFF_SCOPE + TFF_NO_FUNCTION_ARGUMENTS);
  if (name == "SG::VarHandleBase::storeFromHandle") {
    return true;
  }
  return false;
}


const pass_data callcheck_pass_data =
{
  GIMPLE_PASS, /* type */
  "callcheck", /* name */
#if GCC_VERSION < 9000
  0, /* optinfo_flags */
#else
  OPTGROUP_NONE,  /* optinfo_flags */
#endif
  TV_NONE, /* tv_id */
  0, /* properties_required */
  0, /* properties_provided */
  0, /* properties_destroyed */
  0, /* todo_flags_start */
  0  /* todo_flags_finish */
};


class callcheck_pass : public gimple_opt_pass
{
public:
  callcheck_pass (gcc::context* ctxt)
    : gimple_opt_pass (callcheck_pass_data, ctxt)
  { 
  }

  virtual unsigned int execute (function* fun) override
  { return callcheck_execute(fun); }

  unsigned int callcheck_execute (function* fun);

  virtual opt_pass* clone() override { return new callcheck_pass(*this); }
};


unsigned int callcheck_pass::callcheck_execute (function* fun)
{
  // Skip compiler-internal functions.
  if (fun->decl) {
    tree name = DECL_NAME (fun->decl);
    if (name) {
      const char* namestr = IDENTIFIER_POINTER (name);
      if (namestr && namestr[0] == '_' && namestr[1] == '_') {
        // Don't skip constructors and destructors.
        if (// gcc 8+
            !startswith (namestr, "__ct_") && !startswith (namestr, "__dt_") &&
            // gcc 6
            !endswith (namestr, "_ctor ") && !endswith (namestr, "_dtor "))
        {
          return 0;
        }
      }

      // ROOT special case.
      // The ClassDef macro injects this into user classes; the inline
      // definition of this uses static data.
      if (strcmp (namestr, "CheckTObjectHashConsistency") == 0) {
        return 0;
      }
    }
  }

  tree ctx_parm = context_arg (fun->decl);
  if (ctx_parm) {

    basic_block bb;
    FOR_EACH_BB_FN(bb, fun) {
      for (gimple_stmt_iterator si = gsi_start_bb (bb); 
           !gsi_end_p (si);
           gsi_next (&si))
      {
        gimplePtr stmt = gsi_stmt (si);
        if (is_gimple_call (stmt)) {
          tree fndecl = gimple_call_fndecl (stmt);
          if (!fndecl) {
            fndecl = vcall_fndecl (stmt);
          }
          std::string name = decl_as_string (fndecl, TFF_SCOPE + TFF_TEMPLATE_NAME);
          hide_targs (name);
          std::string::size_type ipos = name.find (" [with");
          if (ipos != std::string::npos) {
            name.erase (ipos, std::string::npos);
          }
          auto it = context_fns.find (name);
          if (it != context_fns.end()) {
            if (whitelisted (fun->decl)) continue;
            emit_warning (it->second, stmt, fndecl, ctx_parm);
          }
#if 0
          else {
            fprintf (stderr, "fn %s\n", name.c_str());
          }
#endif
        }
      }
    }
  }

  return 0;
}


} // anonymous namespace


void init_callcheck_checker (plugin_name_args* plugin_info)
{
  struct register_pass_info pass_info = {
    new callcheck_pass(g),
    "ssa",
    0,
    PASS_POS_INSERT_AFTER
  };

  register_callback (plugin_info->base_name,
                     PLUGIN_PASS_MANAGER_SETUP,
                     NULL,
                     &pass_info);
}
