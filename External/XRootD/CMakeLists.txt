# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building XRootD as part of the offline / analysis release.
#

# Set the name of the package:
atlas_subdir( XRootD )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_XROOTD )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building XRootD as part of this project" )

# The source code of XRootD:
set( _source "http://cern.ch/lcgpackages/tarFiles/sources/xrootd-4.8.4.tar.gz" )
set( _md5 "7b43100a6c452a7a30aeb2412761528b" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/XRootDBuild )

# Collect the paths that the XRootD build should consider when looking for
# its externals:
set( _xrootdExternals )
if( LIBXML2_LCGROOT )
   find_package( LibXml2 )
   list( APPEND _xrootdExternals ${LIBXML2_LCGROOT} )
endif()
if( PYTHON_LCGROOT )
   find_package( PythonLibs )
   list( APPEND _xrootdExternals ${PYTHON_LCGROOT} )
endif()
if( ZLIB_LCGROOT )
   find_package( ZLIB )
   list( APPEND _xrootdExternals ${ZLIB_LCGROOT} )
endif()
if( ATLAS_BUILD_PYTHON OR ATLAS_BUILD_LIBXML2 )
   list( APPEND _xrootdExternals ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
endif()

# Extra arguments for the build configuration:
set( _extraArgs )
if( _xrootdExternals )
   list( APPEND _extraArgs
      -DCMAKE_PREFIX_PATH:STRING=${_xrootdExternals} )
endif()
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Create the script that will sanitize xrootd-config after the build:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh @ONLY )

# Build XRootD:
ExternalProject_Add( XRootD
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   PATCH_COMMAND patch -p1 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/xrootd-4.8.4-stringop.patch
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DENABLE_FUSE:BOOL=FALSE -DENABLE_CRYPTO:BOOL=TRUE
   -DENABLE_KRB5:BOOL=TRUE  -DENABLE_READLINE:BOOL=TRUE
   ${_extraArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( XRootD forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of XRootD."
   DEPENDERS download )
ExternalProject_Add_Step( XRootD purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for XRootD"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( XRootD forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of XRootD"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( XRootD buildinstall
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing XRootD into the build area"
   DEPENDEES install )
add_dependencies( Package_XRootD XRootD )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( XRootD Python )
endif()
if( ATLAS_BUILD_LIBXML2 )
   add_dependencies( XRootD LibXml2 )
endif()

# Install XRootD:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
