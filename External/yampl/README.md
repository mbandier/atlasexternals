Yet Another Message Passing Library
===================================

This package builds the yampl library for the offline software of ATLAS.

The library's sources are taken from https://github.com/vitillo/yampl.git.
