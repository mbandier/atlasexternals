# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Package building GDB as part of the offline software build.
#

# The name of the package:
atlas_subdir( Gdb )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Set up a dependency on Python:
find_package( PythonInterp 2.7 REQUIRED )
find_package( PythonLibs 2.7 REQUIRED )
find_package( ZLIB )
find_package( xz )

# Temporary build directory:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GdbBuild )

# Look for makeinfo. As if it's not available, we need to tell the build
# configuration about it.
find_program( _makeinfoExe NAMES makeinfo HINTS ENV PATH )
set( _makeinfoSetting "MAKEINFO=true" )
if( _makeinfoExe )
   set( _makeinfoSetting "MAKEINFO=${_makeinfoExe}" )
endif()

# Create the script that will configure the build of Gdb.
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/configure.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh @ONLY )

# Libraries to link against:
set( _libraries
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}bfd${CMAKE_STATIC_LIBRARY_SUFFIX} )
list( APPEND _libraries
  ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/${CMAKE_STATIC_LIBRARY_PREFIX}iberty${CMAKE_STATIC_LIBRARY_SUFFIX} )

# Set up the build of GDB for the build area:
ExternalProject_Add( Gdb
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/Gdb/gdb-8.1.tar.xz
   URL_MD5 f46487561f9a16916a8102316f7fd105
   PATCH_COMMAND patch -p0 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-8.0.patch
   COMMAND patch -p0 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-bug20020.patch
   COMMAND patch -p1 <
   ${CMAKE_CURRENT_SOURCE_DIR}/patches/gdb-disable-makeinfo.patch
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/configure.sh
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   BUILD_BYPRODUCTS ${_libraries} )
ExternalProject_Add_Step( Gdb forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Gdb"
   DEPENDERS download )
add_dependencies( Package_Gdb Gdb )

# Set up its installation:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/Findgdb.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Build the package's application:
atlas_add_executable( resolveAtlasAddr2Line src/resolveAtlasAddr2Line.cxx
   INCLUDE_DIRS ${CMAKE_INCLUDE_OUTPUT_DIRECTORY} ${ZLIB_INCLUDE_DIRS}
   LINK_LIBRARIES ${_libraries} ${ZLIB_LIBRARIES} ${CMAKE_DL_LIBS} )
add_dependencies( resolveAtlasAddr2Line Gdb )

# Install its other resources:
atlas_install_python_modules( python/__init__.py python/gdbhacks )
atlas_install_scripts( scripts/atlasAddress2Line )

# Clean up.
unset( _buildDir )
unset( _makeinfoExe )
unset( _makeinfoSetting )
unset( _libraries )
