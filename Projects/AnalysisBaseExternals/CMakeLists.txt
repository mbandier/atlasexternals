# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Main configuration file for building the AnalysisBaseExternals project.
#

# The minimum required CMake version:
cmake_minimum_required( VERSION 3.6 FATAL_ERROR )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# Set where to pick up AtlasCMake and AtlasLCG from.
set( AtlasCMake_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
mark_as_advanced( AtlasCMake_DIR LCG_DIR )

# Read in the project's version from a file called version.txt. But let it be
# overridden from the command line if necessary.
file( READ ${CMAKE_SOURCE_DIR}/version.txt _version )
string( STRIP ${_version} _version )
set( ANALYSISBASEEXTERNALS_PROJECT_VERSION ${_version}
   CACHE STRING "Version of the AnalysisBaseExternals project to build" )
unset( _version )

# Find the ATLAS CMake code.
find_package( AtlasCMake REQUIRED )

# Set up the AtlasLCG modules, without setting up an actual LCG release.
set( LCG_VERSION_POSTFIX "" CACHE STRING "The LCG version postfix to use" )
set( LCG_VERSION_NUMBER 0 CACHE STRING "The LCG version number to use" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )

# Make CMake aware of the files in the cmake/ subdirectory.
list( INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake )

# Load the project's build options.
include( ProjectOptions )

# Set up CTest.
atlas_ctest_setup()

# Set up the project with all of its packages.
atlas_project( AnalysisBaseExternals ${ANALYSISBASEEXTERNALS_PROJECT_VERSION}
   PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../../ )

# Configure and install the post-configuration file.
configure_file( ${CMAKE_SOURCE_DIR}/cmake/PostConfig.cmake.in
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake @ONLY )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR} )

# Install the export sanitizer script.
install(
   FILES ${CMAKE_SOURCE_DIR}/cmake/skeletons/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Generate the environment setup for the externals, to be used during the build.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )

# Generate replacement rules for the installed paths.
set( _replacements )
if( NOT "$ENV{NICOS_PROJECT_HOME}" STREQUAL "" )
   get_filename_component( _buildDir $ENV{NICOS_PROJECT_HOME} PATH )
   list( APPEND _replacements ${_buildDir}
      "\${AnalysisBaseExternals_DIR}/../../../.." )
endif()
if( NOT "$ENV{NICOS_PROJECT_RELNAME}" STREQUAL "" )
   list( APPEND _replacements $ENV{NICOS_PROJECT_RELNAME}
      "\${AnalysisBaseExternals_VERSION}" )
endif()

# Now generate and install the installed setup files.
lcg_generate_env(
   SH_FILE ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   REPLACE ${_replacements} )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   DESTINATION . RENAME env_setup.sh )

# Package up the release using CPack.
atlas_cpack_setup()
