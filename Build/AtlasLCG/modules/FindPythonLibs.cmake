# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# This file is here to intercept find_package(PythonLibs) calls, and
# massage the paths produced by the system module, to make them relocatable.
#

# This module requires CMake 3.12.
cmake_minimum_required( VERSION 3.12 )

# The LCG include(s):
include( LCGFunctions )

# Temporarily clean out CMAKE_MODULE_PATH, so that we could pick up
# FindPythonLibs.cmake from CMake:
set( _modulePathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )

# Make the code ignore the system path(s). If we are to pick up Python
# from the release.
if( PYTHON_LCGROOT )
   set( CMAKE_SYSTEM_IGNORE_PATH /usr/include /usr/bin /usr/lib /usr/lib32
      /usr/lib64 )
endif()
lcg_system_ignore_path_setup()

# Option for forcing the usage of Python2 when both Python2 and Python3 are
# available.
option( ATLAS_FORCE_PYTHON2 "Force the usage of Python2" FALSE )

# Decide about the name of the module to use.
set( _moduleName Python )
if( ATLAS_FORCE_PYTHON2 )
   set( _moduleName Python2 )
endif()

# Call CMake's own FindPython.cmake module. It will prefer finding Python3
# over Python2 if it's available, which is generally the behaviour that
# we want, but ATLAS_FORCE_PYTHON2 can be used to force the usage of Python2
# if needed.
find_package( ${_moduleName} COMPONENTS Development )

# Restore CMAKE_MODULE_PATH:
set( CMAKE_MODULE_PATH ${_modulePathBackup} )
unset( _modulePathBackup )
set( CMAKE_SYSTEM_IGNORE_PATH )

# Set the PYTHONLIBS_FOUND variable for backwards compatibility.
if( ${_moduleName}_FOUND )
   set( PYTHONLIBS_FOUND TRUE )
endif()

# Massage the paths found by the system module:
if( ${_moduleName}_FOUND AND NOT GAUDI_ATLAS )

   # Update the include directories:
   set( _newIncludes )
   foreach( _inc ${${_moduleName}_INCLUDE_DIRS} )
      set( _relocatableDir ${_inc} )
      _lcg_make_paths_relocatable( _relocatableDir )
      list( APPEND _newIncludes
         $<BUILD_INTERFACE:${_inc}>
         $<INSTALL_INTERFACE:${_relocatableDir}> )
      unset( _relocatableDir )
   endforeach()
   set( PYTHON_INCLUDE_DIRS ${_newIncludes} )
   unset( _newIncludes )

   # Update the libraries:
   set( _newLibs )
   foreach( _lib ${${_moduleName}_LIBRARIES} )
      set( _relocatableLib ${_lib} )
      _lcg_make_paths_relocatable( _relocatableLib )
      list( APPEND _newLibs
         $<BUILD_INTERFACE:${_lib}>
         $<INSTALL_INTERFACE:${_relocatableLib}> )
      unset( _relocatableLib )
   endforeach()
   set( PYTHON_LIBRARIES ${_newLibs} )
   unset( _newLibs )

   # Set variables used for the environment generation.
   set( PYTHONLIBS_INCLUDE_DIRS ${${_moduleName}_INCLUDE_DIRS} )
   set( PYTHONLIBS_LIBRARY_DIRS ${${_moduleName}_LIBRARY_DIRS} )

elseif( ${_moduleName}_FOUND )
   # When building Gaudi, just use the variables set by CMake's module as they
   # are.
   set( PYTHON_INCLUDE_DIRS ${${_moduleName}_INCLUDE_DIRS} )
   set( PYTHON_LIBRARIES    ${${_moduleName}_LIBRARIES} )
   set( PYTHON_LIBRARY_DIRS ${${_moduleName}_LIBRARY_DIRS} )
endif()

# Set up the RPM dependency.
lcg_need_rpm( Python FOUND_NAME ${_moduleName} VERSION_NAME PYTHON )

# Clean up.
unset( _moduleName )
